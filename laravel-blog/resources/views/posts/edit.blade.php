@extends('layouts.app')

@section('tabName')
    Edit Post
@endsection

@section('content')
	<!-- <h1>{{$post}}</h1> -->

	<form class = "col-10 p-5 mx-auto" method = "POST" action = '/posts/{{$post->id}}/edit'>
		<!-- CSRF stands for Cross-Site Request Forgery. It is form of attact where malicious users may send malicious request while pretending to be authorize user. Laravel uses token to detect if form input request have not been tampered with. -->

		@method('PUT')
		@csrf

   		
		<div class = "form-group">
			<label for = 'title'>Title:</label>
			<input type="text" name="title" class = "form-control" id="title" value="{{$post->title}}"/>
		</div>

		<div class = "form-group">
			<label for = "content">Content:</label>
			<textarea class = "form-control" id = "content" name = "content" rows =3 >{{$post->content}}</textarea>
		</div>

		<div class = 'mt-2 '>
			<button class= "btn btn-primary">Update Post</button>
		</div>
	</form>


@endsection




