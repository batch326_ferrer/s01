@extends('layouts.app')

@section('tabName')
    {{$post->title}}
@endsection

@section('content')
	<!-- <h1>{{$post}}</h1> -->

	<div class = "card col-6 mx-auto">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$post->title}}</h2>
			<p class = "card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class = 'card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
			<h4>Content:</h4>
			<p class = "card-text">{{$post->content}}</p>

			<h5>Likes: {{count($post->likes)}}  Comments: {{count($post->comments)}} </h5>


			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class = "d-inline" method = "POST" action ="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains('user_id',Auth::id()))
							<button class = "btn btn-danger">Unlike</button>
						@else
							<button class = "btn btn-success">Like</button>
						@endif
					</form>					
				@endif

				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">Comment</button>
			@endif
			<br>
			<!-- <a href="/posts" class = "btn btn-info">View all posts</a> -->

			<a href="/posts" >View all posts</a>
			<!-- <h5>{{$post->comments}}</h5> -->

			<h3>Comments:</h3>

			@if($post->comments)
				@foreach($post->comments as $comment)
				    <div class="card text-center mt-2">
				        <div class="card-body">
				            <p class="card-subtitle mb-3">{{$comment->content}}</p>
			                <p class="card-subtitle mb-0 text-end ">Posted By: {{$comment->user->name}}</p>
			                <p class="card-subtitle mb-0 text-end">Posted On: {{$comment->created_at}}</p>
				        </div>
				    </div>
				@endforeach
			@endif

			

			<!-- Modal -->
			<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
			        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			      </div>
			      <div class="modal-body">
			        <!-- <form method="POST" action="/posts/{{$post->id}}/comment"> -->
			        	<form id="commentForm" method="POST" action="/posts/{{$post->id}}/comment" >
			          @csrf <!-- Include CSRF token for security -->
			          <div class="mb-3">
			            <label for="commentText" class="form-label">Comment:</label>
			            <textarea class="form-control" id="comment" name="comment" rows="3" >WRITE COMMENT HERE!</textarea>
			          </div>
			        </form>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
			        <button type="submit" form="commentForm" class="btn btn-primary">Submit</button>
			      </div>
			    </div>
			  </div>
			</div>

		</div>		
	</div>


@endsection
