@extends('layouts.app')

@section('tabName')
    Welcome
@endsection

@section('content')
    <!-- <h1>{{$posts}}</h1> -->

    <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="rounded col-4 mx-auto d-block" alt="...">
    @if(count($posts)>3)
        @php
            $randomPosts = $posts->where('isActive', true)->shuffle()->take(3);
        @endphp


        <!-- <h1>{{Auth::user()}}</h1> -->
        <h3 class= "text-center">Featured Posts</h3>
        @foreach($randomPosts as $post)
            <!-- <h1>{{$post}}</h1> -->
            <div class = "card text-center  mt-2">
                <div class = "card-body">
                    <a href="/posts/{{$post->id}}" class = "card-title mb-3 text-primary">  
                                                                        {{$post->title}}                </a>
                    <p class = "card-subtitle mb-3 text-muted">         Author: {{$post->user->name}}   </p>

                </div>
            </div>
        @endforeach
    @else
        <div>
            <h2>There is no post to show</h2>
            <a href="/posts/create" class = "btn btn-info">Create Post</a>
        </div>
    @endif

@endsection
