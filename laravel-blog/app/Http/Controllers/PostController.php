<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;



class PostController extends Controller
{

    //Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        //to check whether there is an authenticated user or none
        if(Auth::user()){
            //instantiate a new Post Oject from the Post method and then save it in $post variable:
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title=$request->input('title');
            $post->content=$request->input('content');

            //this will get the id of the authenticated user and set it as the foreign jet user_id of the new post.
            $post->user_id=(Auth::user()->id);

            //save the post object in our Post Table;
            $post->save();

            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }

    //controller will return all the the blog posts
    public function showPosts(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::where('isActive', true)->get();

        return view('posts.showPosts')->with('posts', $posts);
    }


    //this controller is for LAravel S02 Activity
    public function welcome(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::where('isActive', true)->get();

        return view('welcome')->with('posts', $posts);
    }


    //action for showing only the posts authored by authenticated user
    public function myPosts(){
        if (Auth::user()){
            // $posts = Auth::user()->posts;
            $posts = Auth::user()->posts()->where('isActive', true)->get();
            return view('posts.showPosts')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }



        //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
    public function show($id){
        
        $post = Post::find($id);
        
        return view('posts.show')->with('post', $post);
    }


    //this controller is for Laravel S03 Activity
    public function editPost($id, Request $request) {
        // Find the post by its ID
        $post = Post::find($id);
        // Redirect to the edit view 
        return view('posts.edit')->with('post', $post);
    }
    public function updatePost($id, Request $request) {
        //Check if the user is authenticated
        if (Auth::user()) {
            // Find the post by its ID
            $post = Post::find($id);

            // If the post exists
            if ($post) {
                // Update post properties with form data
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                // Set the authenticated user's ID as the post's user_id
                $post->user_id = Auth::user()->id;
                // Save the changes
                $post->save();
                // Redirect to the edit view with the updated post
                return redirect('/posts');
            } else {
                // Handle the case where the post doesn't exist
                //return redirect()->route('posts.index')->with('error', 'Post not found');
            }
        } else {
             // Redirect to login if the user is not authenticated
             return redirect('/login');
        }
    }

    //this controller is for Laravel S04 Activity
    public function archive($id) {
        // Find the post by its ID
        $post = Post::find($id);

        $post->isActive = false;
        $post->save();

        // Redirect to the edit view 
        return redirect('/posts');
    }

    public function like($id){
        $post = Post::find($id);
        //check if the author of the post is not the same as the login user
        if ($post->user_id != Auth::user()->id){
            if($post->likes->contains("user_id", Auth::user()->id)){
                //delete the like record made by the user before
                PostLike::where("post_id", $post->id)
                    ->where("user_id", Auth::user()->id)
                    ->delete();
            }else{
                //create a new like recod to like this post
                //instantiate a new PostLike object from the Postlike model
                $postlike = new PostLike;
                //define the properties of the $postlike
                $postlike->post_id = $post->id;
                $postlike->user_id = Auth::user()->id;
                //save this postlike object in the database
                $postlike->save();

            }

        }
        return redirect("/posts/$id");
    }

    public function comment($id, Request $request){
        $post = Post::find($id);
            if($request->input('comment')){
                $postcomment = new PostComment;
                $postcomment->post_id = $post->id;
                $postcomment->user_id = Auth::user()->id;
                $postcomment->content=$request->input('comment');
                $postcomment->save();
            }
        return redirect("/posts/$id");
    }


}

